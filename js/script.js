var globalResponse = [];
var offset = [];
var elements;
var radius;
var angle;
var sangle;
var eangle;
var increment;
$(document).ready(function() {
    $.ajaxSetup({
        xhrFields: {
            withCredentials: true
        }
    });
    $.ajax({
        url: 'https://pmctest.strategydotzero.com/Dashboard/GetProjectDossierData',
        type: 'GET',
        dataType: "json",
        success: function(response) {
            globalResponse = response;
            governanceList = response.governanceList;
            getListsForTurnMenu(governanceList);
        }
    });
});

function getListsForTurnMenu (listName) {
    var dom = '<ul class=\'circle\'>';
    $.each(listName, function(){
        dom = dom + '<li class="liElements">'+ this.Name +'</li>'
    });

    var circle = dom + '</ul>';
    $('#container').html(circle);    
    getVariablesForCircularMenu();
}

function getVariablesForCircularMenu(){
    var container = document.getElementById("container");
    offset = $('#container').offset();
    elements = container.querySelectorAll("li");
    radius = container.offsetWidth ;
    rotateAngle = 360 - ((360 / elements.length) + 90)
    angle = (360 / elements.length) * Math.PI / 180;
    sangle = 0;
    eangle = 0;
    increment = 0;
    rotateIncrement = 0;
    moveElements();
}

function moveElements() {
    var x = 0,
        y = 0,
        itemAngle = 0,
        transform = "";
    $.each(reverse(elements), function(key, value) {
        itemAngle = key * angle + increment;
        var myAngle = rotateAngle;
        x = radius + Math.cos(itemAngle) * radius;
        y = radius + Math.sin(itemAngle) * radius;
        var myAngle = getAngle(x, y) - 90;
        transform = "translate(" + x + "px, " + y + "px) rotate("+myAngle+"deg)";
        this.style.msTransform = transform;
        this.style.webkitTransform = transform;
        this.style.MozTransform = transform;
        this.style.transform = transform;
        var width = ((6.28318 * radius)/elements.length) + "px";
        console.log(width);
    });
	$('.liElements').css('width', width + 'px');
}

document.addEventListener("touchstart",startDrag);
document.addEventListener("mousedown",startDrag);

function startDrag(evt) {
	if(evt.touches === undefined){		
	    sangle = getAngle(evt.pageX, evt.pageY);
	    document.addEventListener("mousemove",moveDrag);
	    document.addEventListener("mouseup",endDrag);
	} else {
		sangle = getAngle(evt.touches[0].pageX, evt.touches[0].pageY);
	    document.addEventListener("touchmove",moveDrag);
	    document.addEventListener("touchend",endDrag);
	}
}

function moveDrag(evt) {
	if(evt.touches === undefined){
		increment = getAngle(evt.pageX, evt.pageY) - sangle + eangle;
	} else {
		increment = getAngle(evt.touches[0].pageX, evt.touches[0].pageY) - sangle + eangle;
	}        
    moveElements();
}

function endDrag(evt) {
    eangle = increment;
    if(evt.touches === undefined){
	    document.removeEventListener("mousemove",moveDrag);
	    document.removeEventListener("mouseup",endDrag);
    } else {
	    document.removeEventListener("touchmove",moveDrag);
	    document.removeEventListener("touchend",endDrag);    	
    }
}

function getAngle(x, y) {
    x = x - offset.left;
    y = y - offset.top;
    return Math.atan2(radius - y, radius - x);
}

function reverse(arrayEle) {
    var temp = [];
    var len = arrayEle.length;
    for (var i = (len - 1) ; i > -1; i--) {
        temp.push(arrayEle[i]);
    }
    return temp;
}