var offset = [];
var elements;
var radius;
var angle;
var sangle;
var eangle;
var increment;

$(document).ready(function() {
    getVariables()
});

function getVariables(){
    var container = document.getElementById("container");
    offset = $('#container').offset();
    elements = container.querySelectorAll("li");
    radius = container.offsetWidth / 2;
    angle = (360 / elements.length) * Math.PI / 180;
    sangle = 0;
    eangle = 0;
    increment = 0;
    moveElements();
}

function moveElements() {
    var x = 0,
        y = 0,
        itemAngle = 0,
        transform = "";
    $.each(elements, function(key, value) {
        itemAngle = key * angle + increment;
        x = radius + Math.cos(itemAngle) * radius;
        y = radius + Math.sin(itemAngle) * radius;
        transform = "translate(" + x + "px, " + y + "px)";
        this.style.msTransform = transform;
        this.style.webkitTransform = transform;
        this.style.MozTransform = transform;
        this.style.transform = transform;
    });
}

document.addEventListener("touchstart",startDrag);

function startDrag(evt) {
    sangle = getAngle(evt.touches[0].pageX, evt.touches[0].pageY);
    document.addEventListener("touchmove",moveDrag);
    document.addEventListener("touchend",endDrag);
}

function moveDrag(evt) {
    increment = getAngle(evt.touches[0].pageX, evt.touches[0].pageY) - sangle + eangle;    
    moveElements();
}

function endDrag(evt) {
    eangle = increment;
    document.removeEventListener("touchmove",moveDrag);
    document.removeEventListener("touchend",endDrag);
}

function getAngle(x, y) {
    x = x - offset.left;
    y = y - offset.top;
    return Math.atan2(radius - y, radius - x);
}